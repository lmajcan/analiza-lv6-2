Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u
svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala,
dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. 