﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjesala2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        string rijec = " ";

        int pokusaji = 15;

        List<Label> crtice = new List<Label>();
       

        void Crta(){
            rijec = RandomRijec();
            char[] znak = rijec.ToCharArray();
            int udaljeno = 325 / znak.Length - 1;
            for (int i = 0; i<znak.Length - 1; i++){
                crtice.Add(new Label());
                crtice[i].Location = new Point((i * udaljeno) + 10, 40);
                crtice[i].Text = "_";
                crtice[i].Parent = groupBox1;
                crtice[i].BringToFront();
                crtice[i].CreateControl();
            }
            label1.Text = "Broj slova: " + (znak.Length - 1).ToString();
            
        }

        string RandomRijec()
        {
            string popisRijeci = System.IO.File.ReadAllText(@"C:\Users\nesqu\source\repos\Vjesala2\rijecizaigru.txt");
            string[] rijeci = popisRijeci.Split('\n');
            Random r = new Random();
            return rijeci[r.Next(0, 50)];

            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            label2.Text = "Preostalih pokusaja: " + pokusaji.ToString();
            
            char slovo = textBox1.Text.ToCharArray()[0];
            if (!char.IsLetter(slovo)){
                MessageBox.Show("Krivi unos! Nije slovo.", "Greska.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
                }
            if (pokusaji == 0) { MessageBox.Show("Niste uspijeli pogoditi.", "Igra je gotova!", MessageBoxButtons.OK); }
            else  if (rijec.Contains(slovo))
            {
                char[] slova = rijec.ToCharArray();
                for (int i = 0; i < slova.Length; i++)
                {
                    if (slova[i] == slovo)
                    {
                        crtice[i].Text = slovo.ToString();
                    }
                }

            }
            else{
                MessageBox.Show("Nema unesenog slova.", "Krivo!", MessageBoxButtons.OK);
                pokusaji--;
                
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RandomRijec();
            Crta();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }
    }
}
